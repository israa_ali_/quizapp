import 'package:flutter/material.dart';

import 'Models/Question.dart';

class Logic {
  List<Question> get questionsList => [
        Question(
            question: "Monaco is the smallest country in the world",
            answer: false),
        Question(
            question: "Alaska is the biggest American state in square miles",
            answer: true),
        Question(
            question: "The unicorn is the national animal of Scotland",
            answer: true),
        Question(
            question: "There are five different blood groups", answer: false),
      ];
  int index = 0;
  List<Widget> results = [
    SizedBox(),
  ];

  int get total => questionsList.length;
  int score = 0;

  int nextQuestion(bool type) {
    if (questionsList[index].answer == type) {
      results.add(Icon(
        Icons.check,
        color: Colors.green[700],
      ));
      score++;
    } else {
      results.add(Icon(
        Icons.close,
        color: Colors.red[900],
      ));
    }

    index++;
    return index;
  }
}
