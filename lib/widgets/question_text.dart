import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class QuestionText extends StatelessWidget {
  const QuestionText({@required this.question});
  final String question;
  @override
  Widget build(BuildContext context) {
    return Text(
      question,
      style: TextStyle(
        fontSize: 20,
        color: Colors.white,
      ),
    );
  }
}
