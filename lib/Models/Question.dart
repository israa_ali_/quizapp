import 'package:flutter/foundation.dart';

class Question {

  const Question({
    @required this.question,
    @required this.answer
  });

  
final String question;
final bool answer;

}