import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:quiz_app/Models/Question.dart';
import 'package:quiz_app/logic.dart';
import 'package:quiz_app/widgets/question_text.dart';

class AppBase extends StatefulWidget {
  @override
  _AppBaseState createState() => _AppBaseState();
}

class _AppBaseState extends State<AppBase> {
  Logic logic = new Logic();
  List<Question> questionsList = Logic().questionsList;
  int index = 0;
  List<Widget> get results => logic.results;
  int get total => questionsList.length;
  int get score => logic.score;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blueGrey[800],
        body: index < 4
            ? Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      QuestionText(
                        question: questionsList[index].question,
                      ),
                      SizedBox(height: 50),
                      SizedBox(
                        height: 50,
                        width: 150,
                        child: RaisedButton(
                          child: Text("True"),
                          onPressed: () =>
                              setState(() => index = logic.nextQuestion(true)),
                          color: Colors.green,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                        width: 20,
                      ),
                      SizedBox(
                        height: 50,
                        width: 150,
                        child: RaisedButton(
                          child: Text("False"),
                          onPressed: () =>
                              setState(() => index = logic.nextQuestion(false)),
                          color: Colors.red[700],
                        ),
                      ),
                      Row(
                        children: results,
                      )
                    ]),
              )
            : Center(
                child: Text(
                  "Your Score is : $score/$total",
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
              ));
  }
}
